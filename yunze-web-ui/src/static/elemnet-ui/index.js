import Vue from 'vue'
import {Card,Scrollbar,Button,Form,FormItem,Input,Collapse,CollapseItem
,RadioGroup} from 'element-ui'

Vue.use(Card).use(Scrollbar).use(Button)
    .use(Form).use(FormItem).use(Input).use(Collapse).use(CollapseItem)
    .use(RadioGroup);