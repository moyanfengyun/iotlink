INSERT INTO `iotdb`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `is_cache`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (null, '销售合同', 对应:客户管理ID, 3, 'Contract', 'yunze/XSGL/Contract/index', 1, 0, 'C', '0', '0', 'yunze:XsglContract:list', 'education', 'admin', '2021-12-07 13:23:38', 'admin', '2022-03-02 15:41:42', '');
INSERT INTO `iotdb`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `is_cache`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (null, '产品入库', 对应:库存管理ID, 1, 'Inout', 'warehouse/Inout/index.vue', 1, 0, 'C', '0', '0', 'warehouse:Inout:list', 'form', 'admin', '2021-11-17 08:39:11', 'admin', '2022-08-03 11:49:15', '');
INSERT INTO `iotdb`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `is_cache`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (null, '库存明细', 对应:库存管理ID, 99, 'Inventorydetails', 'warehouse/Inventorydetails/index.vue', 1, 0, 'C', '0', '0', 'yunze:inventorydetails:list', 'edit', 'admin', '2021-12-24 12:01:01', 'admin', '2022-08-03 11:49:06', '');
INSERT INTO `iotdb`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `is_cache`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (null, '仓库盘点', 对应:仓库管理ID, 99, 'InventoryList', 'warehouse/InventoryList/index.vue', 1, 0, 'C', '0', '0', 'yunze:InventoryList:list', 'table', 'admin', '2021-12-28 14:13:31', 'admin', '2022-01-15 15:05:35', '');
INSERT INTO `iotdb`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `is_cache`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (null, '库存管理', 对应:仓库管理ID, 99, 1, 'Warehousing', NULL, 1, 0, 'M', '0', '0', '', 'input', 'admin', '2022-01-15 15:03:33', 'admin', '2022-01-24 13:50:01', '');
/*
 Navicat Premium Data Transfer

 Source Server         : 主服务器数据库(慎重)
 Source Server Type    : MySQL
 Source Server Version : 80013
 Source Host           : iotlink.mysql.polardb.rds.aliyuncs.com:3306
 Source Schema         : iotdb

 Target Server Type    : MySQL
 Target Server Version : 80013
 File Encoding         : 65001

 Date: 21/09/2023 13:51:03
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for yz_ckgl_commodity
-- ----------------------------
DROP TABLE IF EXISTS `yz_ckgl_commodity`;
CREATE TABLE `yz_ckgl_commodity`  (
  `Cy_ID` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键CCY年月日0001',
  `Cy_Sort` int(11) NOT NULL COMMENT '分类 大分类 单模组 模组加卡 卡 设备',
  `Cy_Category` int(11) NOT NULL COMMENT '类别 小分类',
  `Cy_Name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品名称',
  `Cy_ModelAndNumber` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编号/型号',
  `Cy_Unit` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单位',
  `Cy_Remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `Cy_PurchasePrice` decimal(10, 2) NOT NULL COMMENT '进价',
  `Cy_LowestPrice` decimal(10, 2) NOT NULL COMMENT '最低售价 不能低于 进价',
  `Cy_SuggestedPrice` decimal(10, 2) NOT NULL COMMENT '建议售价',
  `Cy_Supplier` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '供应商 客户表',
  `Operator` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '运营商',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `modified_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最后修改时间',
  `publish_status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '上下架状态：0下架1上架',
  PRIMARY KEY (`Cy_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品表（平台用）' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yz_ckgl_commodity_img
-- ----------------------------
DROP TABLE IF EXISTS `yz_ckgl_commodity_img`;
CREATE TABLE `yz_ckgl_commodity_img`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cmd_id` int(10) UNSIGNED NOT NULL COMMENT '联系人ID',
  `cmd_desc` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图片描述',
  `pic_url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '图片URL',
  `is_master` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否主图：0.非主图1.主图',
  `pic_order` tinyint(4) NOT NULL DEFAULT 0 COMMENT '图片排序',
  `pic_status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '图片是否有效：0无效 1有效',
  `modified_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最后修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '商品表 图片 （平台用）\r\n' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yz_ckgl_commodity_output
-- ----------------------------
DROP TABLE IF EXISTS `yz_ckgl_commodity_output`;
CREATE TABLE `yz_ckgl_commodity_output`  (
  `COT_ID` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `COT_Otid` int(11) NOT NULL COMMENT '出库方式 1.其他\r\n2.领用单出库\r\n3.发货单出库',
  `COT_Time` date NOT NULL COMMENT '出库日期',
  `COT_Use` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用途',
  `COT_Use_People` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '领用人',
  `COT_warehouseman_ID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '库管员',
  PRIMARY KEY (`COT_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品出库信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yz_ckgl_commodity_output_details
-- ----------------------------
DROP TABLE IF EXISTS `yz_ckgl_commodity_output_details`;
CREATE TABLE `yz_ckgl_commodity_output_details`  (
  `CODS_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODS_CyID` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品ID',
  `CODS_Output_Quantity` int(11) NOT NULL COMMENT '本次出库数量',
  `CODS_COTID` int(11) NOT NULL COMMENT '所属商品出库 ID',
  PRIMARY KEY (`CODS_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品出库信息详情表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yz_ckgl_commodity_stock
-- ----------------------------
DROP TABLE IF EXISTS `yz_ckgl_commodity_stock`;
CREATE TABLE `yz_ckgl_commodity_stock`  (
  `CSK_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CSK_CyID` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品ID',
  `CSK_Stock_Quantity` int(11) NOT NULL COMMENT '库存数量',
  PRIMARY KEY (`CSK_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品库存' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yz_ckgl_commodity_storage
-- ----------------------------
DROP TABLE IF EXISTS `yz_ckgl_commodity_storage`;
CREATE TABLE `yz_ckgl_commodity_storage`  (
  `CSE_ID` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CSE_Stid` int(11) NOT NULL COMMENT '入库源 1.采购入库\r\n2.生产入库\r\n3样品入库\r\n4.其他',
  `CSE_Time` date NOT NULL COMMENT '入库日期',
  `CSE_Overview` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '概述',
  `CSE_Manager_ID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '经办人',
  `CSE_warehouseman_ID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '库管员',
  `CSE_fliieUrl` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上传文件地址',
  PRIMARY KEY (`CSE_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品入库信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yz_ckgl_commodity_storage_details
-- ----------------------------
DROP TABLE IF EXISTS `yz_ckgl_commodity_storage_details`;
CREATE TABLE `yz_ckgl_commodity_storage_details`  (
  `CSDS_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CSDS_CyID` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品ID',
  `CSDS_Storage_Quantity` int(11) NOT NULL COMMENT '本次入库数量',
  `CSDS_CSEID` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所属商品入库 ID',
  `CSDS_URL` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上传文件地址',
  PRIMARY KEY (`CSDS_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品入库信息详情表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yz_ckgl_inventorydetails
-- ----------------------------
DROP TABLE IF EXISTS `yz_ckgl_inventorydetails`;
CREATE TABLE `yz_ckgl_inventorydetails`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CaseNumber` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '箱号\r\n',
  `PlateNumber` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '盘号',
  `productCode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '产品编码',
  `arraylNumber` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '序列号',
  `batchNumber` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '批号',
  `Pkg_Id` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '卷盘号',
  `MAC` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'MAC',
  `DeviceCode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '设备码',
  `SerialNumber` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '串号',
  `SN` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'SN',
  `Original_SN` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '原SN',
  `iccid` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'iccid',
  `IMSI` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'IMSI',
  `IMEI` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'IMEI',
  `CMEI` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'CMEI',
  `CSDS_ID` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '商品入库详情表id',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `enteringwarehousetime` datetime(0) NOT NULL COMMENT '入库时间',
  `CSDS_CyID` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '商品入库详情表商品ID',
  `VID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '虚拟编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8001 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '库存明细' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
