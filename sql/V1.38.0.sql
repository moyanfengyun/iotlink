INSERT INTO `iotdb`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `is_cache`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (null, '客户信息', 2126, 1, 'customer', 'yunze/XSGL/customer/index', 1, 1, 'C', '0', '0', 'yunze:XsglCustomer:list', 'people', 'admin', '2021-10-12 09:41:15', 'admin', '2022-01-15 15:36:28', '');
INSERT INTO `demo_iotdb`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `is_cache`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2329, '财务管理', 2125, 27, 'finance', '', 1, 0, 'M', '0', '0', NULL, 'money', 'admin', '2023-08-17 15:17:23', 'admin', '2023-08-17 15:17:33', '');
INSERT INTO `demo_iotdb`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `is_cache`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2330, '公司账户', 2329, 1, 'Firm', 'yunze/XSGL/Firm/index', 1, 0, 'C', '0', '0', 'yunze:CwglFirm:list', 'phone', 'admin', '2023-08-17 15:19:05', 'admin', '2023-08-17 15:19:08', '');

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for yz_cwgl_firm_account
-- ----------------------------
DROP TABLE IF EXISTS `yz_cwgl_firm_account`;
CREATE TABLE `yz_cwgl_firm_account`  (
  `Fat_ID` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Fat_Name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公司名称',
  `Fat_Abbreviation` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '\r\n简称',
  `Fat_Bank` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '开户行',
  `Fat_Bank_Number` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '行号',
  `Fat_Account_Number` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '账号',
  `Fat_Legal_Person` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '法人',
  `Fat_Phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电话',
  `Fat_Address` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地址',
  `state` tinyint(4) NOT NULL COMMENT '状态',
  PRIMARY KEY (`Fat_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '公司账户表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

INSERT INTO `iotdb`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `is_cache`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2105, '客户信息', 2125, 1, 'customer', 'yunze/XSGL/customer/index', 1, 1, 'C', '0', '0', 'yunze:XsglCustomer:list', 'people', 'admin', '2021-10-12 09:41:15', 'admin', '2022-01-15 15:36:28', '');
INSERT INTO `iotdb`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `is_cache`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2210, '客户图片编辑', 2105, 10, '', NULL, 1, 0, 'F', '0', '0', 'yunze:XsglCustomer:guest', '#', 'admin', '2022-03-03 09:41:35', '', NULL, '');
INSERT INTO `iotdb`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `is_cache`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2211, '加载客户图片', 2105, 11, '', NULL, 1, 0, 'F', '0', '0', 'yunze:XsglCustomer:urlPicture', '#', 'admin', '2022-03-03 09:42:14', '', NULL, '');
INSERT INTO `iotdb`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `is_cache`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2212, '客户联系人图片编辑', 2105, 12, '', NULL, 1, 0, 'F', '0', '0', 'yunze:XsglCustomer:Prdedit', '#', 'admin', '2022-03-03 09:42:33', '', NULL, '');
INSERT INTO `iotdb`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `is_cache`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2213, '获取联系人图片', 2105, 13, '', NULL, 1, 0, 'F', '0', '0', 'yunze:XsglCustomer:contacts', '#', 'admin', '2022-03-03 09:42:56', '', NULL, '');
INSERT INTO `iotdb`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `is_cache`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2207, '客户联系记录', 2125, 2, 'CustomerContact', 'yunze/XSGL/CustomerContact/index', 1, 0, 'C', '0', '0', 'yunze:CustomerContact:list', 'online', 'admin', '2022-02-21 15:07:36', 'admin', '2022-03-02 15:41:49', '');